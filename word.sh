#!/bin/bash

# Download Upstream Template
mkdir temp_WORD_data
pushd temp_WORD_data
  wget -O w_WORD_template.zip https://osf.io/w9sbf/download
  unzip w_WORD_template.zip
  cp t2-weighted.nii.gz ../_w_WORD_93.75micron.nii.gz
  cp mask.nii.gz ../_w_WORD_mask_93.75micron.nii.gz
popd
rm -rf temp_WORD_data
gunzip _w_WORD_mask_93.75micron.nii.gz
gunzip _w_WORD_93.75micron.nii.gz

fslorient -setsform .09375 0 0 0 0 .09375 0 0 0 0 .09375 0 0 0 0 1 _w_WORD_93.75micron.nii

# Multiple Sizes
ResampleImage 3 _w_WORD_93.75micron.nii _w_WORD_250micron.nii 0.250x0.250x0.250 0 0
SmoothImage 3 _w_WORD_250micron.nii 0.50 w_WORD_250micron.nii
ResampleImage 3 _w_WORD_mask_93.75micron.nii _w_WORD_mask_250micron_temp.nii 0.250x0.250x0.250 0 0
SmoothImage 3 _w_WORD_mask_250micron_temp.nii 0.50 _w_WORD_mask_250micron.nii
THRESHOLD=$(fslstats w_WORD_250micron.nii -P 1.1)

# Make RAS
for pair in "w_WORD_250micron.nii .250" "_w_WORD_mask_250micron.nii .250"
do
	set -- $pair
	fslorient -setsform ${2} 0 0 -7.6875 0 ${2} 0 -15.75 0 0 ${2} -10.5938 0 0 0 1 ${1}
	fslorient -setsformcode 1 ${1}
	fslorient -copysform2qform ${1}
done

# Make masked templates
fslmaths _w_WORD_mask_250micron.nii -thr ${THRESHOLD} -bin w_WORD_250micron_mask.nii
fslmaths w_WORD_250micron -mas w_WORD_250micron_mask.nii w_WORD_250micron_masked.nii
gzip w_WORD_250micron.nii

# Cleanup
rm _w_WORD*

