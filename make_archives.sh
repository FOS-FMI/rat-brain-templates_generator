#!/usr/bin/env bash
MAKE_MESH=false

while getopts ':v:n:m' flag; do
	case "${flag}" in
		v)
			PV="$OPTARG"
			;;
		n)
			PN="$OPTARG"
			;;
		m)
			MAKE_MESH=true
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

if [ -z "$PV" ]; then
	PV=9999
fi

if [ -z "$PN" ]; then
	PN="rat-brain-templates"
fi

P="${PN}-${PV}"

mkdir ${P}
cp FAIRUSE-AND-CITATION ${P}
pushd ${P}
	bash ../awra.sh || exit 1
	bash ../marpn.sh || exit 1
	bash ../word.sh || exit 1
	bash ../mgu.sh || exit 1
	bash ../word2awra.sh || exit 1
	bash ../marpn2awra.sh || exit 1
	if $MAKE_MESH ; then
		bash ../make_mesh.sh -i w_MARPN_pnd_80_25micron.nii.gz -t 9 -m w_MARPN_pnd_80_25micron_mask.nii.gz  -c -s 20 -a 1 -d beginning -b -x
		bash ../make_mesh.sh -i w_AWRA_50micron.nii.gz -t 28000 -m w_AWRA_50micron_mask.nii.gz -c -s 20 -a 1 -d beginning -b -x
		bash ../make_mesh.sh -i f_MGU_50micron.nii.gz -t 18000 -m f_MGU_50micron_mask.nii.gz -c -s 20 -a 1 -d beginning -b -x
		bash ../make_mesh.sh -i w_WORD_250micron.nii.gz -t 0.6 -m w_WORD_250micron_mask.nii.gz -c -s 20 -a 1 -d beginning -b -x
		bash ../make_mesh.sh -i w_WORD_to_w_AWRA_250micron.nii.gz -t 0.6 -m w_AWRA_250micron_mask.nii.gz -c -s 20 -a 1 -d beginning -b -x
		bash ../make_mesh.sh -i marpn2awra_25micron_masked.nii.gz -t 9 -m w_AWRA_50micron_mask.nii.gz -c -s 20 -a 1 -d beginning -b -x
	fi
popd

tar cfJ "${P}.tar.xz" ${P}
str=$(sha512sum ${P}.tar.xz); echo "${str%% *}" > "${P}.sha512"
rm -rf __pycache__
