#!/bin/bash

STANDALONE=$1

#Standalone: Need to download atlas and transform beforehand
if [ -n "${STANDALONE}" ]; then
        bash word.sh
        bash awra.sh
fi

# Registration call
#SyN registration restricted to 3 Levels and never at full resolution (shrink factor 1, smooothing sigmas 0). This will not affect size of output file,
#but the final displacement field (.h5 file) will not be the correct size. If needed, add a token iteration at full resolution. This will increase memory consumption considerably.
gunzip w_AWRA_250micron.nii.gz
gunzip w_WORD_250micron.nii.gz

antsRegistration \
        --float 1 \
        --collapse-output-transforms 1 \
        --dimensionality 3 \
        --initial-moving-transform [w_AWRA_250micron.nii, w_WORD_250micron.nii, 1 ] \
        --initialize-transforms-per-stage 0 --interpolation Linear --output [ w_WORD_, w_WORD_to_w_AWRA_250micron.nii ] \
        --interpolation Linear \
        \
        --transform Rigid[ 0.5 ] \
        --metric MI[w_AWRA_250micron.nii, w_WORD_250micron.nii, 1, 64, Random, 0.1 ] \
        --convergence [ 400x400x400x200, 1e-9, 10 ] \
        --smoothing-sigmas 3.0x2.0x1.0x0.0vox \
        --shrink-factors 10x4x2x1 \
        --use-estimate-learning-rate-once 0 \
        --use-histogram-matching 1 \
        \
        --transform Affine[ 0.1 ]\
        --metric MI[w_AWRA_250micron.nii, w_WORD_250micron.nii, 1, 64, Regular, 0.1 ] \
        --convergence [ 400x200, 1e-10, 10 ] \
        --smoothing-sigmas 1.0x0.0vox \
        --shrink-factors 2x1 \
        --use-estimate-learning-rate-once 0 \
        --use-histogram-matching 1 \
        \
        --transform SyN[0.25,3,0] \
        --metric CC[w_AWRA_250micron.nii, w_WORD_250micron.nii,1,4] \
        --convergence [100x70x50,1e-6,10] \
        --shrink-factors 8x4x2 \
        --smoothing-sigmas 3x2x1vox \
        \
        --winsorize-image-intensities [ 0.05, 0.95 ] \
        --write-composite-transform 1 \
        --verbose

fslorient -copyqform2sform w_WORD_to_w_AWRA_250micron.nii

#ResampleImage 3 dsurqec_40micron_mask.nii dsurqec_15micron_mask.nii 0.015x0.015x0.015 0 0 1
fslmaths w_WORD_to_w_AWRA_250micron.nii -mas w_AWRA_250micron_mask.nii.gz w_WORD_to_w_AWRA_250micron_masked.nii
gzip w_WORD_to_w_AWRA_250micron.nii
gzip w_AWRA_250micron.nii
gzip w_WORD_250micron.nii

# Clean up
rm w_WORD_Composite.h5
rm w_WORD_InverseComposite.h5

