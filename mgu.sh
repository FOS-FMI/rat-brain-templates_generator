#!/bin/bash

# Download Upstream Template
wget -O Fischer344_nii_v2.zip https://zenodo.org/record/3555556/files/Fischer344_nii_v2.zip?download=1
unzip Fischer344_nii_v2.zip
rm Fischer344_nii_v2.zip
cp Fischer344_nii_v2/Fischer344_template.nii _f_MGU_60micron.nii
cp Fischer344_nii_v2/Fischer344_mask.nii _f_MGU_mask_60micron.nii
rm -rf Fischer344_nii_v2

# Erode 60micron mask
python ../mgu_erode.py

# Multiple sizes
ResampleImage 3 _f_MGU_60micron.nii _f_MGU_50micron.nii 0.050x0.050x0.050 0 0 4
SmoothImage 3 _f_MGU_50micron.nii 0.10 f_MGU_50micron.nii
ResampleImage 3 _f_MGU_60micron.nii _f_MGU_250micron.nii 0.250x0.250x0.250 0 0 4
SmoothImage 3 _f_MGU_250micron.nii 0.50 f_MGU_250micron.nii
ResampleImage 3 _f_MGU_60micron_mask.nii _f_MGU_50micron_mask.nii 0.050x0.050x0.050 0 0 4
SmoothImage 3 _f_MGU_50micron_mask.nii 0.10 f_MGU_50micron_mask.nii
ResampleImage 3 _f_MGU_60micron_mask.nii _f_MGU_250micron_mask_temp.nii 0.250x0.250x0.250 0 0 4
SmoothImage 3 _f_MGU_250micron_mask_temp.nii 0.50 _f_MGU_250micron_mask.nii
THRESHOLD=$(fslstats f_MGU_250micron.nii -P 1.1)

# Make RAS
for pair in "f_MGU_50micron.nii .050" "f_MGU_250micron.nii .250" "f_MGU_50micron_mask.nii .050" "_f_MGU_250micron_mask.nii .250"
do
	set -- ${pair}
	fslorient -setsform ${2} 0 0 -8.46 0 ${2} 0 -16.08 0 0 ${2} -12.18 0 0 0 1 ${1}
	fslorient -setsformcode 1 ${1}
	fslorient -copysform2qform ${1}
done

# Make masked templates
fslmaths f_MGU_50micron -mas f_MGU_50micron_mask.nii f_MGU_50micron_masked.nii
fslmaths _f_MGU_250micron_mask.nii -thr ${THRESHOLD} -bin f_MGU_250micron_mask.nii
fslmaths f_MGU_250micron -mas f_MGU_250micron_mask.nii f_MGU_250micron_masked.nii
gzip f_MGU_50micron_mask.nii
gzip f_MGU_250micron_mask.nii
gzip f_MGU_50micron.nii
gzip f_MGU_250micron.nii
# Cleanup
rm _f_MGU*
