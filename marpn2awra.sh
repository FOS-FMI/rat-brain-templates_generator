#!/bin/sh
gunzip w_AWRA_50micron_masked.nii.gz
gunzip w_MARPN_pnd_80_50micron_masked.nii.gz
gunzip w_MARPN_pnd_80_25micron.nii.gz

# Registration call
antsRegistration \
	--float 1 \
	--collapse-output-transforms 1 \
	--dimensionality 3 \
	--initial-moving-transform [w_AWRA_50micron_masked.nii,w_MARPN_pnd_80_50micron_masked.nii, 1 ] \
	--initialize-transforms-per-stage 0 --interpolation Linear --output [ marpn2awra_, marpn2awra_50micron_masked.nii ] \
	--interpolation Linear \
	\
	--transform Rigid[ 0.5 ] \
	--metric MI[w_AWRA_50micron_masked.nii,w_MARPN_pnd_80_50micron_masked.nii, 1, 64, Random, 0.3 ] \
	--convergence [ 400x400x400x200, 1e-9, 10 ] \
	--smoothing-sigmas 3.0x2.0x1.0x0.0vox \
	--shrink-factors 10x4x2x1 \
	--use-estimate-learning-rate-once 0 \
	--use-histogram-matching 1 \
	\
	--transform Affine[ 0.1 ] \
	--metric MI[w_AWRA_50micron_masked.nii,w_MARPN_pnd_80_50micron_masked.nii, 1, 64, Regular, 0.3 ] \
	--convergence [ 400x200, 1e-10, 10 ] \
	--smoothing-sigmas 1.0x0.0vox \
	--shrink-factors 2x1 \
	--use-estimate-learning-rate-once 0 \
	--use-histogram-matching 1 \
	\
	--transform SyN[0.1,3,0] \
	--metric CC[w_AWRA_50micron_masked.nii,w_MARPN_pnd_80_50micron_masked.nii,1,4] \
	--convergence [100x70x50x20,1e-6,10] \
	--shrink-factors 8x4x2x1 \
	--smoothing-sigmas 3x2x1x0vox \
	\
	--winsorize-image-intensities [ 0.05, 0.95 ] \
	--write-composite-transform 1 \
	--verbose

fslorient -copyqform2sform marpn2awra_50micron_masked.nii

#Use the composite to transform annotation file

antsApplyTransforms -d 3 -i w_MARPN_pnd_80_25micron.nii -r w_AWRA_50micron_masked.nii -o marpn2awra_25micron_masked.nii -t marpn2awra_Composite.h5 -n MultiLabel
fslorient -copyqform2sform marpn2awra_25micron_masked.nii
gzip marpn2awra_50micron_masked.nii
gzip marpn2awra_25micron_masked.nii
gzip w_AWRA_50micron_masked.nii
gzip w_MARPN_pnd_80_50micron_masked.nii
gzip w_MARPN_pnd_80_25micron.nii
# Cleanup

rm marpn2awra_InverseComposite.h5
rm marpn2awra_Composite.h5
