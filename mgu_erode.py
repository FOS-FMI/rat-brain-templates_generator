import sys
from glob import glob
import skimage
from skimage import measure
import nibabel
import numpy
import os
from math import floor
import argparse
from scipy import ndimage

def main():

        #Load mgu mask
        img= nibabel.load('_f_MGU_mask_60micron.nii')
        img_mask = img.get_fdata()
        header=img.header.copy()

        #Erode mgu mask
        img_eroded = ndimage.binary_erosion(img_mask, iterations=7)
        img_nifti=nibabel.Nifti1Image(img_eroded,None,header=header)
        nibabel.save(img_nifti,'_f_MGU_60micron_mask.nii')

if __name__ == '__main__': main()
