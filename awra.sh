#!/bin/bash

# Get data and put it in the current work dir
# if standalone
wget resources.chymera.eu/distfiles/awra_marpn_rat_brains-1.0.tar.xz
tar xvf awra_marpn_rat_brains-1.0.tar.xz
mv awra_marpn_rat_brains-1.0/S56570_gre_masked_DS_affined.nii.tif .
rm -rf awra_marpn_rat_brains-1.0/

# Make RAS
python ../tif_to_nii.py

# Multiple sizes
THRESHOLD=$(fslstats w_AWRA_50micron.nii -P 2)
ResampleImage 3 w_AWRA_50micron.nii _w_AWRA_250micron.nii 0.250x0.250x0.250 0 0 4
SmoothImage 3 _w_AWRA_250micron.nii 0.50 w_AWRA_250micron.nii

# Make RAS
fslorient -setsform 0.05 0 0 -9.92857 0 0.05 0 -23.5 0 0 0.05 -17.4286 0 0 0 1 w_AWRA_50micron.nii
fslorient -setsformcode 1 w_AWRA_50micron.nii
fslorient -copysform2qform w_AWRA_50micron.nii
fslorient -setsform 0.250 0 0 -9.92857 0 0.250 0 -23.5 0 0 0.250 -17.4286 0 0 0 1 w_AWRA_250micron.nii
fslorient -setsformcode 1 w_AWRA_250micron.nii
fslorient -copysform2qform w_AWRA_250micron.nii

# Make masks
fslmaths w_AWRA_50micron.nii -thr ${THRESHOLD} -bin w_AWRA_50micron_mask.nii
fslmaths w_AWRA_50micron -mas w_AWRA_50micron_mask.nii w_AWRA_50micron_masked.nii
fslmaths w_AWRA_250micron.nii -thr ${THRESHOLD} -bin w_AWRA_250micron_mask.nii
fslmaths w_AWRA_250micron -mas w_AWRA_250micron_mask.nii w_AWRA_250micron_masked.nii
gzip w_AWRA_50micron.nii
gzip w_AWRA_250micron.nii

#Cleanup
rm S56570_gre_masked_DS_affined.nii.tif
rm _w*
rm awra_marpn_rat_brains-1.0.tar.xz
