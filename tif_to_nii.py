import numpy as np
from skimage import io
import nibabel as nib

im = io.imread('S56570_gre_masked_DS_affined.nii.tif')

#Reference: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3408821/
#Orientation of the brain is made to match Paxinos and Watson The Rat Brain in Stereotaxic Coordinates
#Assuming original orientation is same handedness as RAS, either no coordinates or two would need to be negated to maintain handedness
im = im.transpose(2, 0, 1)
im = im[...,::-1,::-1]
nii_im = nib.Nifti1Image(im, [[0.05, 0, 0, -9.92857], [0, 0.05, 0, -23.5], [0, 0, 0.05, -17.4286], [0, 0, 0, 1]])
nii_im.header['xyzt_units'] = 2
nib.save(nii_im, "w_AWRA_50micron.nii")
