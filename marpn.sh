#!/bin/bash

# IF STANDALONE: Get data and put it in the current work dir
# Reference: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3639493/
wget resources.chymera.eu/distfiles/awra_marpn_rat_brains-1.0.tar.xz
tar xvf awra_marpn_rat_brains-1.0.tar.xz
mv awra_marpn_rat_brains-1.0/* .
rm -rf awra_marpn_rat_brains-1.0/

# Set affines
declare -A affines
affines[2]="x 0 0 -5.48123 0 x 0 -12.7125 0 0 x -9.68601 0 0 0 1"
affines[4]="x 0 0 -6.0000 0 x 0 -12.9 0 0 x -10.1515 0 0 0 1"
affines[8]="x 0 0 -6.97947 0 x 0 -14.5393 0 0 x -11.89277 0 0 0 1"
affines[12]="x 0 0 -7.65549 0 x 0 -17.073 0 0 x -12.6284 0 0 0 1"
affines[18]="x 0 0 -7.750 0 x 0 -17.100 0 0 x -12.500 0 0 0 1"
affines[24]="x 0 0 -8.08929 0 x 0 -17.8393 0 0 x -12.3598 0 0 0 1"
affines[40]="x 0 0 -8.3123 0 x 0 -17.8929 0 0 x -12.4668 0 0 0 1"
affines[80]="x 0 0 -8.46614 0 x 0 -23.2071 0 0 x -15.821 0 0 0 1"

THRESHOLD=$(fslstats _w_MARPN_pnd_2_25micron.nii -P 2)

for pnd in 2 4 8 12 18 24 40 80
do
	# swap x and y axes
	fslswapdim _w_MARPN_pnd_${pnd}_25micron.nii y x z w_MARPN_pnd_${pnd}_25micron.nii
	gunzip w_MARPN_pnd_${pnd}_25micron.nii.gz

	# Multiple Sizes
	ResampleImage 3 w_MARPN_pnd_${pnd}_25micron.nii _w_MARPN_pnd_${pnd}_50micron.nii 0.050x0.050x0.050 0 0 4
	SmoothImage 3 _w_MARPN_pnd_${pnd}_50micron.nii 0.10 w_MARPN_pnd_${pnd}_50micron.nii
	ResampleImage 3 w_MARPN_pnd_${pnd}_25micron.nii _w_MARPN_pnd_${pnd}_250micron.nii 0.250x0.250x0.250 0 0 4
	SmoothImage 3 _w_MARPN_pnd_${pnd}_250micron.nii 0.50 w_MARPN_pnd_${pnd}_250micron.nii

	# Make RAS
	for size_pair in "25 .025" "50 .05" "250 .25"
	do 
		set -- $size_pair
		fslorient -setsform $(echo ${affines[${pnd}]} | sed "s/x/${2}/g") w_MARPN_pnd_${pnd}_${1}micron.nii
		fslorient -setsformcode 1 w_MARPN_pnd_${pnd}_${1}micron.nii
		fslorient -copysform2qform w_MARPN_pnd_${pnd}_${1}micron.nii

	done

	# Make masks
	fslmaths w_MARPN_pnd_${pnd}_25micron.nii -thr ${THRESHOLD} -bin w_MARPN_pnd_${pnd}_25micron_mask.nii
	gunzip w_MARPN_pnd_${pnd}_25micron_mask.nii.gz
	fslmaths w_MARPN_pnd_${pnd}_25micron -mas w_MARPN_pnd_${pnd}_25micron_mask.nii w_MARPN_pnd_${pnd}_25micron_masked.nii
	gunzip w_MARPN_pnd_${pnd}_25micron_masked.nii.gz

	ResampleImage 3 w_MARPN_pnd_${pnd}_25micron_mask.nii _w_MARPN_pnd_${pnd}_50micron_mask.nii 0.050x0.050x0.050 0 0 4
	SmoothImage 3 _w_MARPN_pnd_${pnd}_50micron_mask.nii 0.10 w_MARPN_pnd_${pnd}_50micron_mask.nii
	fslmaths w_MARPN_pnd_${pnd}_50micron -mas w_MARPN_pnd_${pnd}_50micron_mask.nii w_MARPN_pnd_${pnd}_50micron_masked.nii

	ResampleImage 3 w_MARPN_pnd_${pnd}_50micron_mask.nii _w_MARPN_pnd_${pnd}_250micron_mask.nii 0.250x0.250x0.250 0 0 4
	SmoothImage 3 _w_MARPN_pnd_${pnd}_250micron_mask.nii 0.50 w_MARPN_pnd_${pnd}_250micron_mask.nii
	fslmaths w_MARPN_pnd_${pnd}_250micron -mas w_MARPN_pnd_${pnd}_250micron_mask.nii w_MARPN_pnd_${pnd}_250micron_masked.nii

	gzip w_MARPN_pnd_${pnd}_50micron_mask.nii
        gzip w_MARPN_pnd_${pnd}_50micron.nii
	gzip w_MARPN_pnd_${pnd}_250micron_mask.nii
	gzip w_MARPN_pnd_${pnd}_250micron.nii
	gzip w_MARPN_pnd_${pnd}_25micron.nii
	gzip w_MARPN_pnd_${pnd}_25micron_masked.nii
	gzip w_MARPN_pnd_${pnd}_25micron_mask.nii
done

# Cleanup
rm _w*
rm S56570_gre_masked_DS_affined.nii.tif
rm awra_marpn_rat_brains-1.0.tar.xz
